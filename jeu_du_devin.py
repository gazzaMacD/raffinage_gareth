"""
Objectifs
==============
Exercice 2 : Jeu du devin
Le jeu du devin se joue à deux joueurs. Le premier joueur choisit un nombre compris entre 1 et
999. Le second doit le trouver en un minimum d’essais. À chaque proposition, le premier joueur
indique si le nombre proposé est plus grand ou plus petit que le nombre à trouver. En fin de
partie, le nombre d’essais est donné.
2.1. La machine fait deviner le nombre. Écrire un programme dans lequel la machine choisit un
nombre et le fait deviner à l’utilisateur. Bien sûr, pour écrire ce programme on appliquera la
méthode des raffinages.
TP 2 1/5
Algorithmique et programmation Raffinages
2.2. Saisie robuste. Quand l’utilisateur entre sa proposition, il peut faire une faute de frappe et
taper autre chose qu’un nombre, par exemple : 50x.
2.2.1. Que se passe-t-il dans ce cas ?
2.2.2. En tenant compte de l’observation précédente, modifier le programme pour que la saisie
soit robuste : si l’utilisateur ne saisit pas un entier, on lui explique que c’est un entier qui est
attendu et on lui redemande sa proposition.
2.2.3. Modifier le programme pour que l’utilisateur puisse abandonner la partie en cours en
saisissant 0 quand le programme lui demande de proposer un nombre. On pourra éventuellement
lui demander de confirmer qu’il veut bien abandonner.
2.3. La machine joue. Écrire un programme dans lequel l’utilisateur choisit un nombre et la
machine doit le trouver. Pour chaque nombre proposé, l’utilisateur indique s’il est trop petit ('p'
ou 'P'), trop grand ('g' ou 'G') ou trouvé ('t', 'T'). Bien sûr, pour écrire ce programme on
appliquera la méthode des raffinages.
Indication : On utilisera une recherche par dichotomie pour trouver le nombre. Si le nombre
cherché est compris entre a et b, la machine proposera la valeur médiane m = (a + b)=2. L’intervalle
à considérer étant alors soit [a::m[ (l’intervalle allant de a inclus jusqu’à m exclu), soit
]m::b] (l’intervalle allant de m exclu jusqu’à b inclus).
2.4. Le programme complet. Écrire le programme de jeu qui donne le choix à l’utilisateur entre
deviner ou faire deviner le nombre.
2.5. On peut recommencer. Compléter le programme précédent pour que l’ordinateur propose de
faire une nouvelle partie lorsque la précédente est terminée.

Raffinage
==============
R0
----------
Two player game where one player guesses a number betwwen 1 && 999
choosen by other player, who gives hints of higher or lower until player
guesses correctly or loses due to exceeding number of tries. One player
is user and the other is machine

R1
----------
How can we <<  Create two player game where one player guesses a number betwwen 1 && 999
choosen by other player, who gives hints of higher or lower until player
guesses correctly or loses due to exceeding number of tries.>>

    - Greet user and describe game options
    - Ask user to enter option as integer between 1 and 3, Check for correct entry.
        { 0 < game_option < 4}       game_option: out

    - Choose Option based on 'game_option'
    - If game_option == 1:
        - Choose randomly a positive integer between between 1 and 999.
            {0 < solution < 1000}        solution: out
        - Alert user number has been chosen
        - Set max_tries as variable
            {max_tries == 10 }           max_tries: out
        while loop checking max_tries variable is above 0:
        - Ask user to enter a guess of an integer between 1 & 999, check input
            {0 < user_guess < 1000}    user_guess :    out
        - if correct
            Print congratulations message
            user_guess:     in      result:     out    message:     out
        - if user_guess higher than solution:
            Print message to indicate lower
            user_guess:     in      solution:  in   max_tries:      out
        - if user_guess lower than solution:
            Print message to indicate higher
            user_guess:     in      solution:  in   max_tries:      out
        else statement to indicate loop exited without break
            - Print bad luck message and signal end of game

    - elif game_option == two:
        - Ask user to mentaly choose a positive integer between between 1 and 999.
        - Ask user if number has been choosen with prompt and check input.
            { has_chosen == n or has_chosen == y}     has_chosen: out
        - if has_chosen == n:
            - sleep for some time  to give  user time to choose
            {  is_chosen: 'y' or 'n' }       is_chosen:    out
        - Determine next step based on user choice
            is_chosen:    in
        - Set max_tries  as a variable
        - Calculate machine's guess
          {1 < last_guess < 999}    last_guess : out
        while loop checking max_tries variable is above 0:
            - Display machine's last guess
            last_guess:     out
            - Get user hint as string, check  user input
            user_hint:      out
              displayed correct to user as:  higher, lower as string values
              {user_response}    user_response : out
            if user_hint == 'c':
                - Print congratulations message with number of tries
            if user_hint == 'l':
                - decriment  max_tries
                - set last_guess as lower than the previous value of last_guess
                last_guess:         out
            if user_hint == 'h':
                - decriment  max_tries
                - set last_guess as higher than the previous value of last_guess
                last_guess:         out
        else statement to indicate loop exited without break
            - Print bad luck message and signal end of game
            -  correct or max_tries is reached

    - elif game_option == three:
        - Exit the programme

R2
----------
How can we  "calculate the machines guess"
-   maxium = 999
-   minumum = 1
-   maximum subtract minimum integer divide by  2  and  add  to minumum

How can we  " set last_guess as higher than the previous value of last_guess
- set minimun to same value as last guess
- calculate machine guess as above in  calculate the machine guess"

How can we  " set last_guess as lower than the previous value of last_guess
- set maximum to same value as last guess
- calculate machine guess as above in  calculate the machine guess"

"""


from random import randint
import time

def greeting():
    """
    Greet  user and describe game options
    """
    # Greet user
    print("Welcome to the Number  Guessing Game")
    # Print description of options for user.
    time.sleep(2)
    print(
        "You will be asked three options.\n"
        "In the first option you will try to guess a  number betwwen 1  && "
        "999  the machine chooses. Max number of guesses is 10."
        "\nIn the second option you will choose a number between 1 && 999"
        " and the machine will try to guess your number."
        "Max number of tries  is 10 for the machine"
        "\nThe third option is to quit the game."
        )


def get_option():
    """
    Ask user to enter option as integer between 1 and 3.
    Checks for correct entry.
    return: option:  integer
    """
    # Whie loop to get correct option 
    while True:
        try:
            # Enter game option as positif integer 1, 2, or 3.
            option = int(input(
                "Please choose an input between 1 and 3\n"
                "1\tYou guess the machine's number.\n"
                "2\tThe computer guesses  your number.\n"
                "3\tExit the game.\n> "
                ))
            if not 0 < option < 4:
                raise ValueError()
            break
        except ValueError:
            # Catch ValueError generated  from either not number 1 to 3 or 
            # entering a string. Print message and re enter loop
            print("Sorry, please enter a valid number try again")

    return option


def get_user_guess():
    """
    Ask  user to enter guess, check is integer between 1 and 999
    and returns guess as integer
    return: guess:  integer
    """
    # While loop to get logically correct value for guess
    while True:
        try:
            guess = int(input(
                "Please guess an integer  between 1 and 999\n"
                "> "
                ))
            if not 0 < guess < 999:
                raise ValueError()
            break
        except ValueError:
            # Catch ValueError generated by user entering incorrect number 
            # or a string. Alert user to error  and re-enter loop
            print("Sorry, please enter a valid number and try again")
            time.sleep(1)

    return guess


def get_user_hint():
    """
    Gets input from the user, three inputs possible, checks that input is
    correct and returns the string hint
    return: hint:  string
    """
    # While loop to get logically correct value for hint
    while True:
        try:
            hint = input(
                "Please input the following letters depending on your hint\n"
                "c\t- correct\n"
                "h\t- higher\n"
                "l\t- lower\n"
                "> "
                )
            if hint not in ['c', 'h', 'l']:
                raise ValueError()
            break
        except ValueError:
            # Catch ValueError generated by user entering incorrect string 
            # Alert user to error  and re-enter loop
            print("Sorry, please enter a valid letter, c, h or l. Try again")
            time.sleep(1)

    return hint


def get_has_number():
    """
    Ask user whether they have chosen a number or not
    return: has_number:  string
    """
    while True:
        try:
            has_number = input(
                "Have you chosen a number yet, y or n ?\n"
                "y\t- yes\n"
                "n\t- no\n"
                "> "
                )
            if has_number not in ['y', 'n']:
                raise ValueError()
            elif has_number == 'y':
                break
            elif has_number == 'n':
                print("okay, I will give you 10 more seconds")
                time.sleep(10)
        except ValueError:
            # Catch ValueError generated by user entering incorrect string 
            # Alert user to error  and re-enter loop
            print("Sorry, please enter a valid letter, y or n. Try again")
            time.sleep(1)


def guess_calculator(higher, lower):
    """
    Takes two parameters, higher and lower
    and returns average of two added to the lower value
    parameter:   higher : integer
    parameter:   lower : integer
    return:      higher : integer
    """
    return (higher - lower) // 2 + lower


def proposition(guess):
    """
    Simply takes the quess and prints it in a message
    """
    print(f"Okay then I guess {guess}. How about that?")


def option_one():
    '''
    Machine chooses a positive integer between 1 and 999
    and the user has 10 tries to guess the machine's
    number
    '''
    # Choose randomly an integer between between 1 and 999.
    solution = randint(1, 999)
    # Alert user that number has been chosen 
    print("Number has been choosen, time to start guessing")
    # Set max_tries
    max_tries = 10
    while max_tries > 0:
        # Ask user to enter a guess of an integer between 1 & 999,
        # and check number
        user_guess = get_user_guess()
        if solution == user_guess:
            print(
                    f"Nice, {user_guess} is the correct answer, "
                    f"Bravo!, You finished in {10 - max_tries}"
                )
            break
        elif user_guess > solution:
            max_tries -= 1
            print(f"Try lower, you have {max_tries} guesses remaining")
        elif user_guess < solution:
            max_tries -= 1
            print(f"Try higher, you have {max_tries} guesses remaining")
        else:
            print(
                    "Should not be here, something went wrong "
                    "in option_one call"
                    )
    else:
        print("Sorry you lost the game becuase you had 10 trys")


def option_two():
    '''
    User chooses a positive integer between 1 and 999
    and the machine has 10 tries to guess the user's
    number
    '''
    # Set max_tries
    max_tries = 10
    # set variables maximum and minimum
    maximum = 999
    minimum = 1
    last_guess = guess_calculator(maximum, minimum)
    get_has_number()
    while max_tries > 0:
        proposition(last_guess)
        user_hint = get_user_hint()
        if user_hint == 'c':
            print(f"Nice, {last_guess} was the correct answer, Bravo to me!")
            break
        elif user_hint == 'h':
            max_tries -= 1
            minimum = last_guess
            last_guess = guess_calculator(maximum, minimum)

        elif user_hint == 'l':
            max_tries -= 1
            maximum = last_guess
            last_guess = guess_calculator(maximum, minimum)
    else:
        print("Sorry you lost the game becuase you had 10 trys")

def option_three():
    exit(0)

def guess_number_game():
    '''
    Two player game where one player guesses a number betwwen 1 && 999
    choosen by other player, who gives hints of higher or lower until player
    guesses correctly or loses due to exceeding number of tries. One player
    is user and the other is machine
    '''
    greeting()

    # Enter game option as positif integer 1, 2, or 3.
    # Check number is 1, 2 or 3 and an integer
    time.sleep(2)
    game_option = get_option()
    #Choose game option based on  user choice  
    if game_option == 1:
        # Guess machine's number
        option_one()
    if game_option == 2:
        # Guess user's number 
        option_two()
    if game_option == 3:
        # Exit programme 
        option_three()

if __name__ == "__main__":
    guess_number_game()

