"""
Objectifs
— Écrire des programmes plus ambitieux
— Mettre ne pratique la méthode des raffinage
— (Aborder les exceptions)
Exercice 1 : Réviser les tables de multiplication
On souhaite aider à la révision des tables de multiplication. Le principe est de disposer d’un
programme qui pose 10 multiplications pour une table donnée (le nombre de gauche est celui
de la table, celui de droite est choisi aléatoirement entre 1 et 10). Le programme demande à
l’utilisateur le résultat et lui indique s’il a bien répondu (« Correct » ou « Erreur »). À la fin, il
lui affiche le nombre de bonnes réponses ainsi qu’un message. Le message est :
— « Excellent !» si toutes les réponses sont justes,
— « Très bien. » s’il n’a commis qu’une seule erreur,
— « Bien. » s’il n’a pas fait plus de trois erreurs,
— « Moyen. » s’il a 4, 5 ou 6 bonnes réponses,
— « Il faut retravailler cette table. » s’il a 3 bonnes réponses ou moins et
— « Est-ce que tu l’as fait exprès ?" si toutes les réponses sont fausses.
La table à réviser sera demandée en début de programme (pas de contrôle).
1.1. Pour obtenir un nombre aléatoire, on utilisera la fonction randint du module random. Faire
help('random.randint') sous l’interpréteur Python pour avoir sa documentation.
1.2. Écrire le programme demandé.
1.3. Compléter le programme pour afficher la question pour laquelle l’utilisateur a mis le plus de
temps pour répondre ainsi la moyenne des temps de réponse. On utilisera la fonction time du
module time pour récupérer l’heure actuelle.
1.4. Ajouter la possibilité pour l’utilisateur de continuer à réviser ses tables de multiplications. On
lui posera la question « On continue les révisions (o/n) ? ». Le programme s’arrêtera si l’utilisateur répond « n » ou « N ». Dans le cas, contraire il fait réviser une table choisie par l’utilisateur.
1.5. Modifier le programme pour qu’il ne pose pas deux fois de suite la même multiplication.
"""
from random import randint
import time


def check_your_multiplication_skills():
    """
    This simple program will test your times tables from 1 to 12
    It will ask 10 random  questions on the table you choose
    It will ask the question, then promt a response. Based on your answer
    It will respond 'correct' or 'incorrect'
    After the 10 questions it will print a message'
    It would be useful to print the final total answers and in case
    of incorrect give the correct response
    """
    # Ask the user which mulitiplication table they would like to practice 
    # Check the number is between 1 and 12
    # If the number is not, ask the user  again for a number between 1 or 12
    # If the entry is not a number as for a number betwwwn 1 and 12
    # Store the entry in a variable 'table'
    # Ask ten random questions about multiplication based on which the is based
        # For each question  
        # Randomly select a number between 1 and 12
        # Ask the question
        # Get the answer frpm the user
        # Check the answer agains the correct answer
        # Respone 'correct'  or 'incorrect'
        # Store the answer as correct if correct
    print("\nLet's check your Mulitiplication Tables\n")
    time.sleep(2)
    print("\nYou will be asked which table you would like to practice")
    time.sleep(2)
    print("When the question is asked please enter a number from 1 to 12")
    print(" inclusive indicating the table.\n")
    time.sleep(2)
    # test to see if table = integer
    #checks to see if bettween 1 & 12 
    while True:
        try:
            table = int(
                input("Which multiplication table would you like to practice?: ")
                )
            if not 0 < table <13:
                raise ValueError()
            break
        except ValueError:
            print("Sorry, please enter a valid number from 1 to 12")
    time.sleep(2)
    print("\nGreat, thanks! Let's start learning the {table} times table")
    time.sleep(2)
    questions_remaining = 10
    answers_correct = 0
    while 0 < questions_remaining:
        # Get a random number between  1 and 12
        multiplier = randint(1, 12)
        # store correct answer to the approaching question
        answer = table * multiplier
        # Get the user answer and convert the answer to integer 
        user_answer = int(input(f"{multiplier} x {table} = ?\n> "))
        if user_answer == answer:
            print("Bravo!")
            answers_correct += 1
        else:
            print("Sorry, not correct")

        questions_remaining -= 1



    print(f"num correct = {answers_correct}")
    print("great you finished!")



if __name__ == "__main__":
    check_your_multiplication_skills()

